import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    height: 70px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    margin-top: 250px;

`

export const Input = styled.div`
    width: 30%;
    display: flex;
    flex-direction: column;
    margin: 5px 0;

    input {
        font-size: 16px;
        padding: 15px;
        border: none;
        border-bottom: 1px solid #383743;
        background: none;
        outline: none;
    }

    .leftIcon {
        width: 27px;
        height: 27px;
        position: relative;
        right: 5%;
        bottom: 40px;
    }

    .passwordIcon {
        width: 35px;
        height: 35px;
        position: relative;
        left: 45%;
        bottom: 70px;
    }

    .errorIcon {
        width: 25px;
        height: 25px;
        position: relative;
        left: 90%;
        bottom: 70px;
    }

    button {
        background: none;
        outline: none;
        border: none;

    }
    .naoAutorizado{
        border-bottom: 1px solid #ff0f44;
    }
`

export const InvalidCredentials = styled.div`
    width: 50%;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: -50px;
    font-weight: bold;

    font-size: 12px;
    color: #ff0f44;

`

export const Login = styled.div`
    width: 30%;
    margin-top: 20px;
    display: flex;
    justify-content: center;
    

    button {
        width: 80%;
        background: #57bbbc;
        border: none;
        border-radius: 3.6px;
        font-size: 20px;
        color: #FFF;
        font-weight: bold;
        padding: 20px;
        outline: none;
        cursor: pointer;

        &:hover {
            opacity: 0.7;
        }
    }
`