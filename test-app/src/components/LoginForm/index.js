import React, { useContext, useState } from 'react';
import * as S from './styles';
import { Link } from 'react-router-dom';

import { Context } from '../../context/AuthContext'

import iconEmail from '../../assets/ic-email.png';
import iconCadeado from '../../assets/ic-cadeado.png';
import iconSenhaAberta from '../../assets/ic-senhaAberta.png';
import iconSenhaFechada from '../../assets/ic-senhaFechada.png';
import iconLoginError from '../../assets/ic-loginError.png';

function LoginForm(){
    const UNAUTHORIZED_STATUS = 401
    
    const { authenticated, handleLogin, statusRequest } = useContext(Context);
    const [email, setEmail] = useState();
    const [senha, setSenha] = useState();
    const [mostrarSenha, setMostrarSenha] = useState(false);
    

    console.log(statusRequest);

    return (
        <S.Container>
                <S.Input>
                    <input type="text" placeholder="E-mail" onChange={e => setEmail(e.target.value)} className={statusRequest == UNAUTHORIZED_STATUS ? 'naoAutorizado' : null}/>
                    <img src={iconEmail} alt="E-mail" className="leftIcon"/>
                    {statusRequest == UNAUTHORIZED_STATUS
                     ? <img src={iconLoginError} className="errorIcon"/>
                     : null
                    }
                </S.Input>
                <S.Input>
                    <input type={mostrarSenha ? "text" : "password"} placeholder="Senha" onChange={e => setSenha(e.target.value)} className={statusRequest == UNAUTHORIZED_STATUS ? 'naoAutorizado' : ''}/>
                    <img src={iconCadeado} alt="Cadeado" className="leftIcon"/>
                    {statusRequest == UNAUTHORIZED_STATUS
                     ? <img src={iconLoginError} className="errorIcon"/>
                     : <button type="button" onClick={() => setMostrarSenha(!mostrarSenha)}>
                            <img src={mostrarSenha ? iconSenhaAberta : iconSenhaFechada} className="passwordIcon" alt="MostrarSenha"/>
                       </button>
                    }
                    
                </S.Input>
                <S.InvalidCredentials>
                    {statusRequest == UNAUTHORIZED_STATUS
                     ? <span>Credenciais informadas são inválidas, tente novamente.</span>
                    : null}
                </S.InvalidCredentials>
                <S.Login>
                <button type="button" onClick={() => handleLogin(email, senha)}>ENTRAR</button>
                </S.Login>
        </S.Container>
    )
}

export default LoginForm;