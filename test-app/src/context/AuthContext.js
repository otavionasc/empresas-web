import React, { createContext, useState } from 'react';

import api from '../services/api';
import history from '../history';

const Context = createContext();

function AuthProvider({ children }){
    const [authenticated, setAuthenticated] = useState(false);
    const [headers, setHeaders] = useState();
    const [statusRequest, setStatusRequest] = useState();

    async function handleLogin(email, senha){
        var params = new URLSearchParams();
        
        params.append('email',email);
        params.append('password', senha);

        await api.post('/api/v1/users/auth/sign_in', params)
            .then(res => {
                setHeaders(res.headers);
                setStatusRequest(200);
            })
            .catch(error => {
                setStatusRequest(error.request.status);
            });

        localStorage.setItem('headers', JSON.stringify(headers));
        api.defaults.headers.Authorization = `Bearer ${headers}`;
        setAuthenticated(true);
        history.push(statusRequest == 200 ? '/home' : '/');
    }

    return(
        <Context.Provider value={{ authenticated, handleLogin, statusRequest }}>
            {children}
        </Context.Provider>
    );
}

export { Context, AuthProvider };