import axios from 'axios';

export default axios.create({
  baseURL: 'https://empresas.ioasys.com.br',
});