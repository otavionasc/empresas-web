import React, { useState, useEffect } from 'react';

import api from '../../services/api';

function Home(){
    const [empresas, setEmpresas] = useState([]);

    useEffect(() => {
        (async () => {
          const { data } = await api.get('/api/v1/enterprises/1');
          setEmpresas(data);
        })();
      }, []);
    
    return <h1>LOGIN REALIZADO COM SUCESSO</h1>
}

export default Home;