import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    height: 70px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    margin-top: 300px;

`
export const Info = styled.div`
    width: 30%;
    height: 70px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    img {
        //object-fit: contain;
    }

    h1 {
        width: 200px;
        font-size: 1.5rem;
        text-align: center;
        margin-top: 70px;
    }

    h2 {
        width: 80%;
        font-size: 1.125rem;
        text-align: center;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: -0.25px;
        text-align: center;
        color: #383743;
    }
`