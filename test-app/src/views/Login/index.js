import React from 'react';
import * as S from './styles';

import logo from '../../assets/logo-home.png'

import LoginForm from '../../components/LoginForm';

function Login(){
    return (
        <S.Container>
           {/* Cabeçalho do login, com logo e mensagem de boas vindas */}
            <S.Info>
                <img src={logo} />
                <h1>BEM-VINDO AO EMPRESAS</h1>
                <h2>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</h2>
            </S.Info>
            <LoginForm />
        </S.Container>
    )
}

export default Login;