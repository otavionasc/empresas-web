import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Login from '../views/Login';
import Home from '../views/Home';

export default function Routes(){
    return(
        <Switch>
            <Route exact path="/" component={Login}/>
            <Route exact path="/home" component={Home} />
        </Switch>
    )
}